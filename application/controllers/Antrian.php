<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Antrian extends CI_Controller {
	
	// public function __construct(){
	// 	parent::__construct();
	// 	$this->load->model('SiswaModel'); // Load SiswaModel ke controller ini
	// }
	
	public function index(){
		$this->load->view('dokter/antrian');
	}


	public function resep(){
		$this->load->view('dokter/resep');
	}

}
