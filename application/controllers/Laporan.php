<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		// $this->load->model('SiswaModel'); // Load SiswaModel ke controller ini
	}
	
	public function bulanan(){
		$this->load->view('dokter/lpr_bulanan');
	}
	public function harian(){
		$this->load->view('dokter/lpr_harian');
	}
}
