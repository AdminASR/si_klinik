<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msr_dokter extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		// $this->load->model('SiswaModel'); // Load SiswaModel ke controller ini
	}
	
	public function index(){
		$this->load->view('dokter/msr_dokter');
	}
}
